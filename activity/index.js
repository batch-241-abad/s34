
const express = require("express");

const app = express(); 
const port = 3000;
app.use(express.json());

let users = [
    {
        "username": "pat1",
        "password": "pat123"
    }
];

app.use(express.urlencoded({extended:true}));

app.get("/home", (request, response) => {
	response.send("Hello to the home page");
});

// A GET method request and response from an ExpressJS API to retrieve data.
app.get("/users", (request, response) => {
    response.send(users);
});


// SIGNUNP

app.post("/signup", (request, response) => {

    if(request.body.username !== '' && request.body.password !== ''){

        users.push(request.body);

        response.send(`User ${request.body.username} successfully registered!`);

    } else {

        response.send("Please input BOTH username and password.");

    }

});


// DELETE method request and response from an ExpressJS API to delete a resource.

app.delete("/delete-user", (request, response) => {
        let message;

        for(let i=0; i<users.length; i++){
            if(request.body.username == users[i].username){
                users[i].password = request.body.password;

                message = `User ${request.body.username}'s has been deleted`;
                break;
            }else{
                message  = "user does not exist"
            }
        }
        response.send(message);
})


app.listen(port, () => console.log(`Server running at port ${port}`));